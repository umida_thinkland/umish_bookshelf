import React from 'react';
import {BrowserRouter as Router, Routes, Route} from 'react-router-dom'
import HomeScreen from './pages/HomeScreen';
import RegisterScreen from './pages/Register';

function App() {

  return (
    <React.StrictMode>
      <Router>
        {/* <Header/> */}
        <main>
          <Routes>
            <Route exact path='/' element={<HomeScreen/>}/>
            <Route path='/register' element={<RegisterScreen/>}/>
          </Routes> 
        </main>
        {/* <Footer/> */}
      </Router> 
    </React.StrictMode>  
  );
}

export default App;
