import axios from 'axios'
import md5 from 'crypto-js/md5';

const baseUrl = "https://no23.lavina.tech"
const signature = (method, url, secret, body) => {
    let sign = body ? 
            method + 'http://mydomain.com' + url + body + secret 
        : method + 'http://mydomain.com' + url + secret
    console.log(sign)
    return md5(sign).toString()
}


export const register = (name, email, key, secret) => async (dispatch) => {
    try {
        dispatch({
            type: 'USER_REGISTER_REQUEST'
        })

        const config = {
            headers: {
                'Content-type': 'application/json'
            }
        }

        const { data } = await axios.post(
            `${baseUrl}/signup`,
            { 'name': name, 'email': email, 'key': key, 'secret': secret },
            config
        )
        
        dispatch({
            type: 'USER_REGISTER_SUCCESS',
            payload: data
        })

        localStorage.setItem('userInfo', JSON.stringify(data))

    } catch (error) {
        dispatch({
            type: 'USER_REGISTER_FAIL',
            payload: error.response && error.response.data.detail
                ? error.response.data.detail
                : error.message,
        })
    }
}

export const getUserDetails = () => async (dispatch, getState) => {
    try {
        dispatch({
            type: 'USER_DETAILS_REQUEST'
        })

        const { userRegister: { userInfo }} = getState()

        const config = {
            headers: {
                'Key': userInfo.data.key,
                'Sign': md5(`GET${baseUrl}/myself${userInfo.data.secret}`).toString(),
                'Content-type': 'application/json'
            }
        }
        
        
        const { data } = await axios.get(
            `${baseUrl}/myself`,
            config
        )
        dispatch({
            type: 'USER_DETAILS_SUCCESS',
            payload: data
        })
    } catch (error) {
        console.log(error)
        dispatch({
            type: 'USER_DETAILS_FAIL',
            payload: error.response && error.response.data.detail
                ? error.response.data.detail
                : error.message,
        })
    }
}

export const getBooks = () => async (dispatch, getState) => {
    try {
        dispatch({
            type: 'USER_DETAILS_REQUEST'
        })

        const { userRegister: { userInfo }} = getState()

        const config = {
            headers: {
                'Key': userInfo.data.key,
                'Sign': md5(`GET${baseUrl}/books${userInfo.data.secret}`).toString(),
                'Content-type': 'application/json'
            }
        }
        
        
        const { data } = await axios.get(
            `${baseUrl}/books`,
            config
        )
console.log(data)
        dispatch({
            type: 'USER_DETAILS_SUCCESS',
            payload: data
        })
    } catch (error) {
        console.log(error)
        dispatch({
            type: 'USER_DETAILS_FAIL',
            payload: error.response && error.response.data.detail
                ? error.response.data.detail
                : error.message,
        })
    }
}

export const createBook = (isbn) => async (dispatch, getState) => {
    try {
        dispatch({
            type: 'CREATE_BOOK_REQUEST'
        })

        const { userRegister: { userInfo }} = getState()
        
        const config = {
            headers: {
                'Key': userInfo.data.key,
                'Sign': md5(`POST${baseUrl}/books{isbn:"${isbn}"}${userInfo.data.secret}`).toString(),
                'Content-type': 'application/json'
            }
        }
        
        
        const { data } = await axios.post(
            `${baseUrl}/books`,
            {isbn},
            config
        )
        
        dispatch({
            type: 'CREATE_BOOK_SUCCESS',
            payload: data
        })
    } catch (error) {
        console.log(error)
        dispatch({
            type: 'CREATE_BOOK_FAIL',
            payload: error.response && error.response.data.message
                ? error.response.data.message
                : error.message,
        })
    }
}

export const changeStatus = (id,status) => async (dispatch, getState) => {
    try {
        dispatch({
            type: 'USER_DETAILS_REQUEST'
        })

        const { userRegister: { userInfo }} = getState()

        const config = {
            headers: {
                'Key': userInfo.data.key,
                'Sign': md5(`PATCH${baseUrl}/books/${id}{"status":${status}}${userInfo.data.secret}`).toString(),
                'Content-type': 'application/json'
            }
        }
        
        
        const { data } = await axios.get(
            `${baseUrl}/books/${id}`,
            {status},
            config
        )
console.log(data)
        dispatch({
            type: 'USER_DETAILS_SUCCESS',
            payload: data
        })
    } catch (error) {
        console.log(error)
        dispatch({
            type: 'USER_DETAILS_FAIL',
            payload: error.response && error.response.data.detail
                ? error.response.data.detail
                : error.message,
        })
    }
}

export const deleteBook = (id) => async (dispatch, getState) => {
    try {
        dispatch({
            type: 'USER_DETAILS_REQUEST'
        })

        const { userRegister: { userInfo }} = getState()

        const config = {
            headers: {
                'Key': userInfo.data.key,
                'Sign': md5(`DELETE${baseUrl}/books/${id}${userInfo.data.secret}`).toString(),
                'Content-type': 'application/json'
            }
        }
        
        
        const { data } = await axios.delete(
            `${baseUrl}/books/${id}`,
            config
        )
console.log(data)
        dispatch({
            type: 'USER_DETAILS_SUCCESS',
            payload: data
        })
    } catch (error) {
        console.log(error)
        dispatch({
            type: 'USER_DETAILS_FAIL',
            payload: error.response && error.response.data.detail
                ? error.response.data.detail
                : error.message,
        })
    }
}