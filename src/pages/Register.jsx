import React, { useState, useEffect } from 'react'
import { Link, useNavigate } from 'react-router-dom'
import { useSelector, useDispatch } from 'react-redux'
import { register } from '../redux/actions'
import { Box, TextField, Checkbox } from '@mui/material';
import LoadingButton from '@mui/lab/LoadingButton';

function RegisterScreen() {
  const [name, setName] = useState('')
  const [email, setEmail] = useState('')
  const [key, setKey] = useState('')
  const [secret, setSecret] = useState('')

  const dispatch = useDispatch()
  const navigate = useNavigate()

	const { error, loading, userInfo } = useSelector(state => state.userRegister)

	useEffect(()=> {
		if(userInfo){
			navigate('/')
		}
	}, [userInfo, navigate])

	const submitHandler = (e) => {
		e.preventDefault()
        dispatch(register(name, email, key, secret))
	}

  return (
    <div className="bg-[url('./assets/images/library.jpg')] bg-cover h-screen relative">
        <div className='absolute top-0 left-0 bottom-0 right-0 bg-[#00000080] flex justify-center items-center'>
            <div className='bg-white rounded p-7 w-1/4'>
                <h1 className='text-3xl text-center mb-4 font-bold'>Sign Up</h1>

                <Box
                    component="form"
                    autoComplete="off"
                    onSubmit={submitHandler}
                >
                    <div className='flex flex-col gap-5'>
                        <TextField 
                            error={error ? true : false}
                            required
                            label="Name" 
                            variant="outlined" 
                            value={name} 
                            onChange={(e) => setName(e.target.value)} 
                            size='small'
                        />
                        <TextField 
                            error={error ? true : false}
                            required
                            label="Email" 
                            variant="outlined" 
                            value={email} 
                            onChange={(e) => setEmail(e.target.value)} 
                            size='small'
                        />
                        <TextField 
                            error={error ? true : false}
                            required
                            label="Key" 
                            variant="outlined" 
                            value={key} 
                            onChange={(e) => setKey(e.target.value)} 
                            size='small'
                        />
                        <TextField
                            error={error ? true : false} 
                            required
                            label="Secret" 
                            variant="outlined" 
                            value={secret} 
                            onChange={(e) => setSecret(e.target.value)} 
                            size='small'
                        />

                        <div>
                            <Checkbox required />
                            <label>I agree to the <Link to='' className='font-semibold'>Terms of User</Link></label>
                        </div>
                        
                        { error && <p className='text-red-600'>{error}!</p> }

                        <LoadingButton
                            type='submit'
                            variant="contained"
                            loading={loading}
                            loadingIndicator="Loading…"
                        >
                            Sign Up
                        </LoadingButton>

                    </div>
                </Box>
            </div>
        </div>
	</div>
  )
}

export default RegisterScreen